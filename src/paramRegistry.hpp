#ifndef MIDIBOX_PARAMREG_H
#define MIDIBOX_PARAMREG_H

#include <EEPROM.h>
#include "config.h"

#ifdef __cplusplus
extern "C" {
#endif   

    void decodeSerialParams(uint8_t *baud, uint8_t *polarity);
    void encodeSerialParams(uint8_t baud, uint8_t polarity);
    int32_t paramReg_getParamValue(uint32_t paramId);
    int32_t paramReg_setParamValue(uint32_t paramId, uint32_t paramValue);
    int32_t paramReg_formatEEPROM();
    int32_t paramReg_init();
    void writeEEPROM();

#ifdef __cplusplus
}
#endif

#endif // MIDIBOX_PARAMREG_H