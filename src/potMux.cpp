#include "potMux.hpp"

#define EMA_ALPHA 0.4
#define DIFF_ABS 6

static bool firstRound = true;
static bool isInit = false;

static uint8_t muxPinS0, muxPinS1, muxPinS2, muxPinS3, muxPinSIG, muxPinSense;

static void (*potChangeHandler)(uint16_t, uint16_t) = NULL;

typedef struct pot {
    uint16_t lastVal;
    uint16_t currentVal;
} pot_t;

pot_t potMux[POT_MUX_NUM_POT];

int32_t potMux_init(uint8_t pinS0, uint8_t pinS1, uint8_t pinS2, uint8_t pinS3,
                    uint8_t pinSIG,  uint8_t pinSense,
                    void (*valChangeHandler)(uint16_t, uint16_t))
{   
    muxPinS0 = pinS0;
    muxPinS1 = pinS1;
    muxPinS2 = pinS2;
    muxPinS3 = pinS3;
    muxPinSIG = pinSIG;
    muxPinSense = pinSense;

    potChangeHandler = valChangeHandler;

    for (uint8_t p = 0; p < POT_MUX_NUM_POT; p++) {
        potMux[p].currentVal = 0;
        potMux[p].lastVal = 0;
    }
    firstRound = true;
    isInit = true;
    return RET_OK;
}

int32_t potMux_read()
{
    if ((!isInit) || (!digitalRead(muxPinSense))) {
        return RET_KO;
    }

    for (uint8_t p = 0; p < POT_MUX_NUM_POT; p++) {
        digitalWrite(muxPinS0, p&1);
        digitalWrite(muxPinS1, (p&2)>>1);
        digitalWrite(muxPinS2, (p&4)>>2);
        digitalWrite(muxPinS3, (p&8)>>3);
        // allow analog signal to become stable
        delayMicroseconds(60);
        uint16_t val = (uint16_t) analogRead(muxPinSIG);

        if (!firstRound) {
            potMux[p].currentVal = (uint16_t)( (float)val*(EMA_ALPHA) + (float)potMux[p].currentVal*(1-EMA_ALPHA));
        } else {
            potMux[p].currentVal = val;
            potMux[p].lastVal = val;
        }

        uint16_t diffAbs = (uint16_t)abs((int32_t)potMux[p].lastVal - (int32_t)potMux[p].currentVal);
        if ((diffAbs > DIFF_ABS) && (potChangeHandler != NULL)){
            potChangeHandler(p, potMux[p].currentVal);
            potMux[p].lastVal = potMux[p].currentVal;
        }

    }
    firstRound = false;
    return RET_OK;
}
