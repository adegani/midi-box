// #include <Arduino.h>
// #include <SPI.h>

#include "config.h"
#include "potMux.hpp"
#include "menuManager.hpp"
#include "paramRegistry.hpp"
#include "midiFSM.hpp"
#include "midiDefs.hpp"
#include "logger.hpp"

elapsedMillis ledOnMillis;

static uint32_t rs232Baud = RS232_DAFAULT_BAUDRATE;
static uint8_t rs232Polarity = RS232_DAFAULT_POLARITY;
static bool potEnable = MUX_DEFAULT_ENABLED;
static uint8_t potChannel = MUX_DEFAULT_CHANNEL;
static uint8_t potBaseCc = MUX_DEFAULT_BASE_CC*16;

static bool resetRS232 = false;

// --- MIDI FSM ---
midiFSM_t midiFSM_DIN;
midiFSM_t midiFSM_RS232;

void handleSysEx(const uint8_t* sysExData, uint16_t sysExSize, bool complete)
{ 
    uint8_t sysExId = sysExData[1];
    uint8_t sysExCommand = sysExData[2];

    MIDIBOX_LOG_DEBUG("SYSEX->len:%d,id:%d,cmd:%d", sysExSize, sysExId, sysExCommand);


    char msg[TEXT_MAX_CHAR];
    uint8_t charCount=0;
    for (uint8_t c=0; (c < sysExSize-4) && (c < TEXT_MAX_CHAR); c++) {
        msg[c] = sysExData[c+3];
        charCount++;
    }
    
    msg[charCount] = '\0'; 
    if (sysExId == MIDI_SYSEX_ID) {
        switch(sysExCommand) {
            case MIDI_SYSEX_SHOW_MESSAGE:
                MIDIBOX_LOG_INFO("Message to display: %s", msg);
                menuManager_showMsg(msg);
                break;
            default:
                break;
        }
    }
}

void handleSysEx_USB(const uint8_t* sysexBuffer, uint16_t byteCount, bool complete)
{
    handleSysEx(sysexBuffer, byteCount, complete);
    for (uint16_t b = 0; b < byteCount; b++) {
        RS232Serial.write(sysexBuffer[b]);
        MIDISerial1.write(sysexBuffer[b]);
        MIDISerial2.write(sysexBuffer[b]);
    }
}

void handleMessage_DIN(midiFSM_event_t *e)
{
    menuManager_AddMidiEvent(e->type, e->data1, e->data2, e->channel);
    if (e->type == MIDI_PROGRAM_CHANGE) {
        usbMIDI.sendProgramChange(e->data1, e->channel, 0);
    } else if (e->type == MIDI_CHANNEL_PRESS) {
        usbMIDI.sendAfterTouch(e->data1, e->channel, 0);
    } else {
        usbMIDI.send(e->type, e->data1, e->data2, e->channel, 0);
    }
}

void handleSysex_DIN(uint8_t *sysexBuffer, uint16_t byteCount, bool complete)
{
    handleSysEx(sysexBuffer, byteCount, complete);
    usbMIDI.sendSysEx(byteCount, sysexBuffer, true);
}

void handleRT_DIN(uint8_t type)
{
    usbMIDI.sendRealTime(type, 0);
}

void handleMessage_RS232(midiFSM_event_t *e)
{
    handleMessage_DIN(e);
}

void handleRT_RS232(uint8_t type)
{
    handleRT_DIN(type);
}

void handleSysex_RS232(uint8_t *sysexBuffer, uint16_t byteCount, bool complete)
{
    handleSysex_DIN(sysexBuffer, byteCount, complete);
}

void signalActivity(bool activity)
{
    static volatile bool ledOn = false;
    if (activity && !ledOn) {
        digitalWriteFast(PIN_LED_SIGNAL, HIGH); // LED on
        ledOnMillis = 0;
        ledOn = true;
    }
    if (ledOnMillis > 5) {
        digitalWriteFast(PIN_LED_SIGNAL, LOW);  // LED off
        ledOn = false;
    }
}

void setBaud(uint8_t baud)
{
    paramReg_setParamValue(PARAM_BAUD_IDX, (uint32_t)baud);
    switch(baud){
        // TODO: metti i valori in un array
        case 0:
            rs232Baud = 115200;
            break;
        case 1:
            rs232Baud = 57600;
            break;
        case 2:
            rs232Baud = 38400;
            break;
        case 3:
            rs232Baud = 31250;
            break;
        case 4:
            rs232Baud = 19200;
            break;
        case 5:
            rs232Baud = 9600;
            break;
        case 6:
            rs232Baud = 4800;
            break;
        case 7:
            rs232Baud = 2400;
            break;
        default:
            rs232Baud = RS232_DAFAULT_BAUDRATE;
    } 
    resetRS232 = true;
    MIDIBOX_LOG_INFO("setBaud %d", baud);
}

uint8_t getBaud()
{
    uint8_t ret = (uint8_t)paramReg_getParamValue(PARAM_BAUD_IDX);
    switch(ret){
        // TODO: metti i valori in un array
        case 0:
            rs232Baud = 115200;
            break;
        case 1:
            rs232Baud = 57600;
            break;
        case 2:
            rs232Baud = 38400;
            break;
        case 3:
            rs232Baud = 31250;
            break;
        case 4:
            rs232Baud = 19200;
            break;
        case 5:
            rs232Baud = 9600;
            break;
        case 6:
            rs232Baud = 4800;
            break;
        case 7:
            rs232Baud = 2400;
            break;
        default:
            rs232Baud = RS232_DAFAULT_BAUDRATE;
    }
    resetRS232 = true;
    MIDIBOX_LOG_INFO("getBaud %d", ret);
    return ret;
}

void setPolarity(uint8_t pol)
{
    paramReg_setParamValue(PARAM_POLARITY_IDX, (uint32_t)pol);
    rs232Polarity = pol;
    resetRS232 = true;
    MIDIBOX_LOG_INFO("setPolarity %d", pol);
}

uint8_t getPolarity()
{
    uint8_t ret = (uint8_t)paramReg_getParamValue(PARAM_POLARITY_IDX);
    rs232Polarity = ret;
    resetRS232 = true;
    MIDIBOX_LOG_INFO("getPolarity %d", ret);
    return ret;
}

void setPotEn(uint8_t potEn)
{
    paramReg_setParamValue(PARAM_POT_EN_IDX, (uint32_t)potEn);
    potEnable = potEn;
    MIDIBOX_LOG_INFO("setPotEn %d", potEn);
}

uint8_t getPotEn()
{
    uint8_t ret = (uint8_t)paramReg_getParamValue(PARAM_POT_EN_IDX);
    potEnable = ret;
    MIDIBOX_LOG_INFO("getPotEn %d", ret);
    return ret;
}

void setPotCh(uint8_t potCh)
{
    paramReg_setParamValue(PARAM_POT_CH_IDX, (uint32_t)potCh);
    potChannel = potCh;
    MIDIBOX_LOG_INFO("setPotCh %d", potCh);
}

uint8_t getPotCh()
{
    uint8_t ret = (uint8_t)paramReg_getParamValue(PARAM_POT_CH_IDX);
    potChannel = ret;
    MIDIBOX_LOG_INFO("getPotCh %d", ret);
    return ret;
}

void setPotBaseCc(uint8_t potBase)
{
    paramReg_setParamValue(PARAM_POT_BASE_IDX, (uint32_t)potBase);
    potBaseCc = potBase*16;
    MIDIBOX_LOG_INFO("setPotBaseCc %d", potBase);
}

uint8_t getPotBaseCc()
{
    uint8_t ret = (uint8_t)paramReg_getParamValue(PARAM_POT_BASE_IDX);
    potBaseCc = ret*16;
    MIDIBOX_LOG_INFO("getPotBaseCc %d", ret);
    return ret;
}

int32_t init_RS232()
{
    MIDIBOX_LOG_DEBUG("Enable MIDI over RS232 (baud: %d, pol %d)", rs232Baud, rs232Polarity);
    RS232Serial.setRX(RS232_SERIAL_RX_PIN);
    RS232Serial.setTX(RS232_SERIAL_TX_PIN);
    if (rs232Polarity == 0) {
        RS232Serial.begin(rs232Baud,SERIAL_8N1);
    } else {
        RS232Serial.begin(rs232Baud,SERIAL_8N1_RXINV_TXINV);
    }
    
    RS232Serial.clear();

    return RET_OK;
}

int32_t initSerial()
{
    // Debug serial (CDC)
    Serial.begin(115200);

    // MIDI 1 serial
    MIDIBOX_LOG_DEBUG("Enable MIDI port 1");
    MIDISerial1.setRX(MIDI_SERIAL1_RX_PIN);
    MIDISerial1.setTX(MIDI_SERIAL1_TX_PIN);
    MIDISerial1.begin(MIDI_SERIAL_BAUDRATE);
    MIDISerial1.clear();
    
    // MIDI 2 serial
    MIDIBOX_LOG_DEBUG("Enable MIDI port 2");
    MIDISerial2.setRX(MIDI_SERIAL2_RX_PIN);
    MIDISerial2.setTX(MIDI_SERIAL2_TX_PIN);
    MIDISerial2.begin(MIDI_SERIAL_BAUDRATE);
    MIDISerial2.clear();
    
    // RS232 serial
    init_RS232();

    return RET_OK;
}

int32_t initIO()
{
    pinMode(PIN_MENU_SELECT, INPUT_PULLUP);
    pinMode(PIN_MENU_OK, INPUT_PULLUP);  
    pinMode(PIN_LED_SIGNAL, OUTPUT);
    pinMode(PIN_MUX_S0, OUTPUT);
    pinMode(PIN_MUX_S1, OUTPUT);
    pinMode(PIN_MUX_S2, OUTPUT);
    pinMode(PIN_MUX_S3, OUTPUT);
    pinMode(PIN_MUX_SENSE, INPUT_PULLDOWN);
    
    return RET_OK;
}

void faultLoop()
{
    // infinite loop with blinking LED
    while(1)
    {
        digitalWrite(PIN_LED_SIGNAL, HIGH);
        delay(500);
        digitalWrite(PIN_LED_SIGNAL, LOW);
        delay(500);
    }
}

void midiSendToRS232(uint8_t type, uint8_t data1, uint8_t data2, uint8_t channel)
{
    RS232Serial.write(type+channel);
    RS232Serial.write(data1);
    if (!((type == MIDI_PROGRAM_CHANGE) || (type == MIDI_CHANNEL_PRESS))) {
        RS232Serial.write(data2);
    }
}

void midiSendToMIDI2(uint8_t type, uint8_t data1, uint8_t data2, uint8_t channel)
{
    MIDISerial2.write(type+channel);
    MIDISerial2.write(data1);
    if (!((type == MIDI_PROGRAM_CHANGE) || (type == MIDI_CHANNEL_PRESS))) {
        MIDISerial2.write(data2);
    }
}

void midiSendToMIDI1(uint8_t type, uint8_t data1, uint8_t data2, uint8_t channel)
{
    MIDISerial1.write(type+channel);
    MIDISerial1.write(data1);
    if (!((type == MIDI_PROGRAM_CHANGE) || (type == MIDI_CHANNEL_PRESS))) {
        MIDISerial1.write(data2);
    }
}

void midiSendToAll(uint8_t type, uint8_t data1, uint8_t data2, uint8_t channel)
{
    usbMIDI.send(type, data1, data2, channel+1, 0);

    midiSendToMIDI1(type, data1, data2, channel);
    midiSendToMIDI2(type, data1, data2, channel);
    midiSendToRS232(type, data1, data2, channel);
}

void handlePotChange(uint16_t potId, uint16_t potVal) {
    if (potEnable) {
        MIDIBOX_LOG_DEBUG("pot %d: %d", potId, potVal);
        signalActivity(true);
        uint8_t midiVal = 127-(potVal >> 3);
        uint8_t midiData1 = potId + potBaseCc;
        uint8_t midiData2 = midiVal;
        midiSendToAll(usbMIDI.ControlChange, midiData1, midiData2, potChannel);
    }
}

void setup()
{
    // init I/O and peripherals
    int32_t retval = 0;
    int32_t all_retval = 0;
    retval = initSerial();
    all_retval |= retval;
    MIDIBOX_LOG_INFO("Serials enabled");
    MIDIBOX_LOG_INFO("Init menu manager...");
    retval = menuManager_init();
    MIDIBOX_LOG_DEBUG("return:%d", retval);
    all_retval |= retval;
    MIDIBOX_LOG_INFO("Init IO...");
    retval = initIO();
    MIDIBOX_LOG_DEBUG("return:%d", retval);
    all_retval |= retval;
    retval = potMux_init(PIN_MUX_S0, PIN_MUX_S1, PIN_MUX_S2, PIN_MUX_S3,
                         PIN_MUX_SIG, PIN_MUX_SENSE, handlePotChange);
    MIDIBOX_LOG_DEBUG("return:%d", retval);
    all_retval |= retval;
    MIDIBOX_LOG_INFO("Init param registry...");
    retval = paramReg_init();
    MIDIBOX_LOG_DEBUG("return:%d", retval);
    all_retval |= retval;
    if (retval)
    {
        MIDIBOX_LOG_ERROR("EEPROM ERROR. Formatting.");
        paramReg_formatEEPROM();
    }
    
    // MIDI handlers and FSM
    usbMIDI.setHandleSystemExclusive(handleSysEx_USB);
    midiFSM_init(&midiFSM_DIN, handleMessage_DIN, handleSysex_DIN, handleRT_DIN);
    midiFSM_init(&midiFSM_RS232, handleMessage_RS232, handleSysex_RS232, handleRT_RS232);

    menuManager_addMenuItem("RS232kBaud", setBaud, getBaud);
    menuManager_addItemOption(0, "115.2");
    menuManager_addItemOption(0, "57.6");
    menuManager_addItemOption(0, "38.4");
    menuManager_addItemOption(0, "31.25");
    menuManager_addItemOption(0, "19.2");
    menuManager_addItemOption(0, "9.6");
    menuManager_addItemOption(0, "4.8");
    menuManager_addItemOption(0, "2.4");

    menuManager_addMenuItem("RS232polarity", setPolarity, getPolarity);
    menuManager_addItemOption(1, "Down");
    menuManager_addItemOption(1, "Up");

    menuManager_addMenuItem("EXT.Potenable", setPotEn, getPotEn);
    menuManager_addItemOption(2, "No");
    menuManager_addItemOption(2, "Yes");

    menuManager_addMenuItem("EXT.PotCH#", setPotCh, getPotCh);
    menuManager_addItemOption(3, "CH: 1");
    menuManager_addItemOption(3, "CH: 2");
    menuManager_addItemOption(3, "CH: 3");
    menuManager_addItemOption(3, "CH: 4");
    menuManager_addItemOption(3, "CH: 5");
    menuManager_addItemOption(3, "CH: 6");
    menuManager_addItemOption(3, "CH: 7");
    menuManager_addItemOption(3, "CH: 8");
    menuManager_addItemOption(3, "CH: 9");
    menuManager_addItemOption(3, "CH:10");
    menuManager_addItemOption(3, "CH:11");
    menuManager_addItemOption(3, "CH:12");
    menuManager_addItemOption(3, "CH:13");
    menuManager_addItemOption(3, "CH:14");
    menuManager_addItemOption(3, "CH:15");
    menuManager_addItemOption(3, "CH:16");

    menuManager_addMenuItem("EXT.PotbaseCC", setPotBaseCc, getPotBaseCc);
    menuManager_addItemOption(4, "CC: 0");
    menuManager_addItemOption(4, "CC:16");
    menuManager_addItemOption(4, "CC:32");
    menuManager_addItemOption(4, "CC:48");
    menuManager_addItemOption(4, "CC:64");
    menuManager_addItemOption(4, "CC:80");
    menuManager_addItemOption(4, "CC:96");

    // read all params from EEPROM and init the GUI menu
    menuManager_readAll();
    
    // if there are errors, go to infinte loop with blinking LED
    if (all_retval != RET_OK)
    {
        MIDIBOX_LOG_ERROR("Unable to complete setup:%d", all_retval);
        menuManager_showErrorCode(all_retval);
        faultLoop();
    }
}

bool readButtons()
{
    bool btnPressed = false;
    if (digitalRead(PIN_MENU_SELECT) == LOW)
    {
        menuManager_handleButtonPress(PIN_MENU_SELECT, millis());
        btnPressed = true;
    }
    if (digitalRead(PIN_MENU_OK) == LOW)
    {
        menuManager_handleButtonPress(PIN_MENU_OK, millis());
        btnPressed = true;
    }
    return btnPressed;
}

void loop()
{
    bool evtReceived = false;

    while(usbMIDI.read()) {
        evtReceived = true;

        // if the event is not a Sysex (or other special event)
        if (usbMIDI.getType() < usbMIDI.SystemExclusive) {
            menuManager_AddMidiEvent(usbMIDI.getType(), usbMIDI.getData1(),
                                 usbMIDI.getData2(), usbMIDI.getChannel());
        
            midiSendToMIDI1(usbMIDI.getType(), usbMIDI.getData1(),
                            usbMIDI.getData2(), usbMIDI.getChannel()-1);
            midiSendToMIDI2(usbMIDI.getType(), usbMIDI.getData1(),
                            usbMIDI.getData2(), usbMIDI.getChannel()-1);
            midiSendToRS232(usbMIDI.getType(), usbMIDI.getData1(),
                            usbMIDI.getData2(), usbMIDI.getChannel()-1);
        }
    }
    
    while(MIDISerial1.available()) {
        uint8_t byte = MIDISerial1.read();
        
        midiFSM_handleByte(&midiFSM_DIN, byte);
        // MIDISerial1.write(byte);
        MIDISerial2.write(byte);
        RS232Serial.write(byte);
        evtReceived = true;
    }
    
    while(RS232Serial.available()) {
        uint8_t byte = RS232Serial.read();
        midiFSM_handleByte(&midiFSM_RS232, byte);
        
        MIDISerial1.write(byte);
        MIDISerial2.write(byte);
        evtReceived = true;
    }

    evtReceived |= readButtons();

    signalActivity(evtReceived);
    menuManager_show(millis());
    if (potEnable) {
        potMux_read();
    }
    writeEEPROM();

    if (resetRS232) {
        init_RS232();
        resetRS232 = false;
    }
}
