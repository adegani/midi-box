#ifndef MIDIBOX_CONFIG_H
#define MIDIBOX_CONFIG_H

#include <Arduino.h>

// Logo bitmap
// #include "logo.h"
// Hardware config
#include "config_hw.h"
#include "MIDI_implementation.h"

// --- VERSION ---
#define MIDIBOX_VERSION "0.4.0"

// --- LOG LEVEL ---
// see: logger.hpp
#define MIDIBOX_LOG_LEVEL MIDIBOX_LOG_LEVEL_DEBUG

// --- CONFIG PRAMS ---
#define PARAM_MODE_IDX      0
#define PARAM_MONITOR_IDX   1
#define PARAM_METRONOME_IDX 2
#define PARAM_MESSAGE_IDX   3
#define PARAM_BAUD_IDX      4
#define PARAM_POLARITY_IDX  5
#define PARAM_POT_EN_IDX    6
#define PARAM_POT_CH_IDX    7
#define PARAM_POT_BASE_IDX  8
#define PARAM_REG_NUMPARAM  9 // Update to the last index + 1

#define PARAM_MODE_DEFAULT      0
#define PARAM_MONITOR_DEFAULT   0
#define PARAM_METRONOME_DEFAULT 0
#define PARAM_MESSAGE_DEFAULT   0
#define PARAM_BAUD_DEFAULT      5
#define PARAM_POLARITY_DEFAULT  RS232_DAFAULT_POLARITY
#define PARAM_POT_EN_DEFAULT    MUX_DEFAULT_ENABLED
#define PARAM_POT_CH_DEFAULT    MUX_DEFAULT_CHANNEL
#define PARAM_POT_BASE_DEFAULT  MUX_DEFAULT_BASE_CC

// --- RETURN VALUES ---
#define RET_OK 0
#define RET_KO -1

// --- FAULT CODE ---
#define FAULT_EEPROM_INIT   0x101
#define FAULT_EEPROM_WRITE  0x102
#define FAULT_EEPROM_READ   0x104


#endif // MIDIBOX_CONFIG_H
