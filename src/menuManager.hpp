#ifndef MIDIBOX_MENU_MANAGER_H
#define MIDIBOX_MENU_MANAGER_H

#include "config.h"
// #include <Wire.h>
// #include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define BADGE_NEXT_ITEM (26)
#define BADGE_NEXT_CFG (16)
#define BADGE_SAVE (31)
#define BADGE_CFG (15)

#define TEXT_MAX_CHAR 9

#define TEMP_TIMEOUT_MS 5000
#define GUI_REFRESH_RATE_MS 100

#define MAX_MENU_ITEM_CHAR_LEN 15
#define MAX_MENU_ITEM_NUM 5
#define MAX_MENU_OPTION_CHAR_LEN 6
#define MAX_MENU_OPTION_NUM 16

#ifdef __cplusplus
extern "C" {
#endif
    int32_t menuManager_init();
    int32_t menuManager_addMenuItem(const char *name, void (*setHandler)(uint8_t), uint8_t (*getHandler)());
    int32_t menuManager_addItemOption(uint16_t itemIdx, const char *name);

    int32_t menuManager_handleButtonPress(uint32_t buttonId, uint32_t timestamp);
    int32_t menuManager_show(uint32_t timestamp);

    int32_t menuManager_showMsg(const char *msg);

    int32_t menuManager_readAll();

    void menuManager_showErrorCode(int32_t errorCode);
    void menuManager_AddMidiEvent(uint8_t type, uint8_t data1, uint8_t data2, uint8_t channel);

#ifdef __cplusplus
}
#endif

#endif // MIDIBOX_MENU_MANAGER_H
