#ifndef MIDIBOX_POT_MUX_H
#define MIDIBOX_POT_MUX_H

#include <Arduino.h>
#include "config.h"

#ifndef POT_MUX_NUM_POT
#define POT_MUX_NUM_POT 16
#endif

#ifdef __cplusplus
extern "C" {
#endif
    int32_t potMux_init(uint8_t pinS0, uint8_t pinS1, uint8_t pinS2,
                        uint8_t pinS3, uint8_t pinSIG, uint8_t pinSense,
                        void (*valChangeHandler)(uint16_t, uint16_t));
    int32_t potMux_read();
#ifdef __cplusplus
}
#endif

#endif // MIDIBOX_POT_MUX_H
