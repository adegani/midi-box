#include "menuManager.hpp"

static Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

// --- some basic structs ---
typedef struct item_option {
    char displayVal[MAX_MENU_OPTION_CHAR_LEN];
} item_option_t;

typedef struct menu_item {
    char name[MAX_MENU_ITEM_CHAR_LEN];
    void (*setHandler)(uint8_t) = NULL;
    uint8_t (*getHandler)() = NULL;
    item_option_t options[MAX_MENU_OPTION_NUM];
    uint8_t optionCount = 0;
    uint8_t currentOption = 0;
} menu_item_t;

typedef struct menu {
    menu_item_t item[MAX_MENU_ITEM_NUM];
    uint8_t itemCount = 0;
    uint8_t currentItem = 0;
} menu_t;

static menu_t myMenu;

// --- GUI finite state machine ---
enum gui_state_e {GUI_STATE_HOME, GUI_STATE_MENU, GUI_STATE_TEMP_MESSAGE};
static gui_state_e guiCurrState = GUI_STATE_HOME;

// --- HOME mode ---
enum gui_home_mode_e {GUI_HOME_MODE_MIDIMON=0, GUI_HOME_MODE_METRONOME, GUI_HOME_MODE_TOT};
static gui_home_mode_e homeCurrState = GUI_HOME_MODE_MIDIMON;

// globals
static bool guiNeedsUpdate = true;
static bool configModPending = false;
static uint32_t lastButtonPressTimestamp = 0;
static uint32_t lastUpdateTimestamp = 0;

static uint8_t lastEvtType;
static uint8_t lastEvtdata1;
static uint8_t lastEvtdata2;
static uint8_t lastEvtchannel;

void printMIDIMonitor()
{
    display.setTextSize(1);
    display.setTextColor(SSD1306_WHITE);
    display.setCursor(1, 1);
    display.println("MIDI Monitor");
    // display.setTextSize(1);
    // display.println("00:Non,65,127");
    // display.println("00:Noff,65,128");
    // display.println("01:SYSx,128,127");
    const char *evtStr;
    switch (lastEvtType)
    {
        case 144:
            evtStr = "NoteOn";
            break;
        case 128:
            evtStr = "NoteOff";
            break;
        case 176:
            evtStr = "CtrlCng";
            break;
        case 192:
            evtStr = "ProgCng";
            break;
        case 224:
            evtStr = "PBend";
            break;
        default:
            evtStr = "UNKNOWN";
    }
    display.setTextSize(2);
    display.printf("%02d:%s\n", lastEvtchannel, evtStr);
    display.setTextSize(1);
    display.printf("%d, %d", lastEvtdata1, lastEvtdata2);
}

void printMetronome()
{
    display.setTextSize(1);
    display.setTextColor(SSD1306_WHITE);
    display.setCursor(1, 1);
    display.println("Metronome");

}

static void noEntryError()
{  
    display.clearDisplay();
    display.setCursor(1,1);
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.println("ERROR: NO MENU ENTRY defined");
    display.println("Please reboot");
    display.display();
}

static void displayTempMessage(const char* msg)
{
    guiCurrState = GUI_STATE_TEMP_MESSAGE;
    display.clearDisplay();
    display.setCursor(1,1);
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.println(msg);
    display.display();
}

static void showLogo()
{
    // display.drawBitmap(0, 0, MIDIBoxLogo, 128, 32, WHITE);
    // display.display();

    display.clearDisplay();
    display.setCursor(1,1);
    display.setTextSize(3);
    display.setTextColor(WHITE);
    display.print("midi");
    display.setTextColor(BLACK);
    display.fillRect(70,0,58,24,WHITE);
    display.println("BOX");
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.print("v");
    display.print(MIDIBOX_VERSION);
    display.display();
    display.display();

    delay(3000);
    // guiTestChar();
}

int32_t menuManager_init()
{
    // Init SSD1306 OLED I2C display
    display.begin(SSD1306_SWITCHCAPVCC, SSD1306_I2C_ADDR, false);
    // Rotate upside-down the screen (the display is mounted rotated)
    display.setRotation(2);
    // Clear the buffer
    display.clearDisplay();

    showLogo();

    display.cp437(true);

    guiCurrState = GUI_STATE_HOME;

    myMenu.itemCount = 0;
    myMenu.currentItem = 0;
    for (uint16_t i = 0; i < MAX_MENU_ITEM_NUM; i++) {
        //myMenu.item[i].name = "NULL";
        strcpy(myMenu.item[i].name, "NULL");
        myMenu.item[i].setHandler = NULL;
        myMenu.item[i].getHandler = NULL;
        myMenu.item[i].optionCount = 0;
        myMenu.item[i].currentOption = 0;
        for (uint16_t e = 0; e < MAX_MENU_OPTION_NUM; e++) {
            //myMenu.item[i].options[e].display_val = "NULL";
            strcpy(myMenu.item[i].options[e].displayVal, "NULL");
        }
    }

    return RET_OK;
}

static void saveCurrentParameter()
{
    uint8_t co = myMenu.item[myMenu.currentItem].currentOption;
    if (myMenu.item[myMenu.currentItem].setHandler != NULL) {
        myMenu.item[myMenu.currentItem].setHandler(co);
    } else {
        displayTempMessage("no setHandler");
    }
}

static void configNextPressed()
{
    if (configModPending) {
        saveCurrentParameter();
    } else {
        myMenu.currentItem = (myMenu.currentItem + 1)%myMenu.itemCount;
    }
    configModPending = false;
}

static void configOkPressed()
{
    uint16_t ca = myMenu.item[myMenu.currentItem].currentOption;
    uint16_t acount = myMenu.item[myMenu.currentItem].optionCount;
    myMenu.item[myMenu.currentItem].currentOption = (ca + 1) % acount;
    configModPending = true;
}

static void homeNextPressed()
{
    homeCurrState = (gui_home_mode_e)((homeCurrState+1)%GUI_HOME_MODE_TOT);
}

static void homeOkPressed()
{
    guiCurrState = GUI_STATE_MENU;
}

int32_t menuManager_handleButtonPress(uint32_t buttonId, uint32_t timestamp)
{
    if (timestamp-lastButtonPressTimestamp > DEBOUNCE_TIME_MS) {
        switch (guiCurrState) {
            case GUI_STATE_HOME:
                if (buttonId == PIN_MENU_SELECT) {
                    homeOkPressed();
                }
                if (buttonId == PIN_MENU_OK) {
                    homeNextPressed();
                } 
                break;
            case GUI_STATE_MENU:
                if (buttonId == PIN_MENU_SELECT) {
                    configNextPressed();
                }
                if (buttonId == PIN_MENU_OK) {
                    configOkPressed();
                } 
                break;
            case GUI_STATE_TEMP_MESSAGE:
                guiCurrState = GUI_STATE_HOME;
                break;
            default:
                break;
        }
        lastButtonPressTimestamp = timestamp; 
        guiNeedsUpdate = true;
    } else {
        // Ignore button press
    }
    return RET_OK;
}

static void setUpperBadge(uint8_t badge)
{
    display.setCursor(120, 1);
    display.setTextSize(1);
    display.write(badge);
}

static void setLowerBadge(uint8_t badge)
{
    display.setCursor(120, 25);
    display.setTextSize(1);
    display.write(badge);
}

static void showCurrentItem()
{
    uint16_t ca = myMenu.item[myMenu.currentItem].currentOption;
    display.setTextSize(1);
    display.setTextColor(SSD1306_WHITE);
    display.setCursor(1, 1);
    display.print(myMenu.item[myMenu.currentItem].name);
    //display.write(26); // Left arrow
    if (configModPending)
    {
        display.print(" *");
    }
    display.setTextSize(2);
    display.setCursor(0, 10);
    if (myMenu.item[myMenu.currentItem].optionCount > 0) {
        display.println(myMenu.item[myMenu.currentItem].options[ca].displayVal);
    } else {
        display.println("INVALID");
    }

    if (configModPending) {
        setUpperBadge(BADGE_SAVE);
    } else {
        setUpperBadge(BADGE_NEXT_ITEM);
    }
    setLowerBadge(BADGE_NEXT_CFG);
}

void showMenu()
{
    if (guiNeedsUpdate) {
        display.clearDisplay();
        if (myMenu.itemCount > 0) {
            showCurrentItem();
        } else {
            noEntryError();
        }
        display.display();
        display.display();
        guiNeedsUpdate = false;
    }
}

void showHome()
{
    if (guiNeedsUpdate) {
        display.clearDisplay();
        switch(homeCurrState) {
            case GUI_HOME_MODE_METRONOME:
                printMetronome();
                break;
            case GUI_HOME_MODE_MIDIMON:
            default:
                printMIDIMonitor();
        }
        setUpperBadge(BADGE_SAVE);
        setLowerBadge(BADGE_NEXT_ITEM);
        display.display();
        display.display();
        guiNeedsUpdate = false;
    }
}

static void changeState(gui_state_e nextState, uint32_t timestamp)
{
    guiCurrState = nextState;
    guiNeedsUpdate = true;
}

int32_t menuManager_show(uint32_t timestamp)
{
    int32_t elapsedStateChange = timestamp-lastButtonPressTimestamp;

    bool timeout = elapsedStateChange > TEMP_TIMEOUT_MS;
    bool timeoutNotInHome = (guiCurrState != GUI_STATE_HOME) && timeout;
    bool refreshElapsed = (timestamp-lastUpdateTimestamp) > GUI_REFRESH_RATE_MS;
    if ((guiNeedsUpdate || timeoutNotInHome) && refreshElapsed){
        switch (guiCurrState) {
            case GUI_STATE_HOME:
                showHome();
                break;
            case GUI_STATE_MENU:
                if (!timeout) {
                    showMenu();
                } else {
                    changeState(GUI_STATE_HOME, timestamp);
                }
                break;
            case GUI_STATE_TEMP_MESSAGE:
                if (timeout) {
                    changeState(GUI_STATE_HOME, timestamp);
                }
                break;
            default:
                break;
        }
        lastUpdateTimestamp = timestamp;
    }
    return RET_OK;
}

int32_t menuManager_addMenuItem(const char *name, void (*setHandler)(uint8_t), uint8_t (*getHandler)())
{
    int32_t retVal = RET_KO;
    
    if ((myMenu.itemCount < MAX_MENU_ITEM_NUM) && (strlen(name) < MAX_MENU_ITEM_CHAR_LEN)) {
        strcpy(myMenu.item[myMenu.itemCount].name, name);
        myMenu.item[myMenu.itemCount].setHandler = setHandler;
        myMenu.item[myMenu.itemCount].getHandler = getHandler;
        myMenu.itemCount++;
        retVal = RET_OK;
    }
    return retVal;
}

int32_t menuManager_addItemOption(uint16_t itemIdx, const char *name)
{
    int32_t retVal = RET_KO;
    if ((itemIdx < myMenu.itemCount) && (strlen(name) < MAX_MENU_OPTION_CHAR_LEN)){
        menu_item_t *currItem = &myMenu.item[itemIdx];
        item_option_t *currOption = &currItem->options[currItem->optionCount];
        strcpy(currOption->displayVal, name);
        currItem->optionCount++;
    retVal = RET_OK;
    }
    return retVal;
}

int32_t menuManager_readAll()
{
    int32_t retVal = RET_KO;
    for (uint16_t i = 0; i < MAX_MENU_ITEM_NUM; i++) {
        if (myMenu.item[i].getHandler != NULL) {
            uint8_t valIdx = myMenu.item[i].getHandler();
            myMenu.item[i].currentOption = valIdx;
            retVal = RET_OK;
        }
    }
    return retVal;
}

void menuManager_AddMidiEvent(uint8_t type, uint8_t data1, uint8_t data2, uint8_t channel)
{
    lastEvtType = type;
    lastEvtdata1 = data1;
    lastEvtdata2 = data2;
    lastEvtchannel = channel;
    //guiNeedsUpdate = true;
    if ((homeCurrState == GUI_HOME_MODE_MIDIMON) && (lastEvtType != 0xF0)) {
        // do not update if not in MIDI monitor and the event is a SYSEX
        guiNeedsUpdate = true;
    }
}

void menuManager_showErrorCode(int32_t errorCode)
{
    display.clearDisplay();
    display.setCursor(1,1);
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.print("ERROR: ");
    display.println(errorCode);
    display.println("Please reboot");
    display.display();
    display.display();
}

int32_t menuManager_showMsg(const char *msg)
{
    display.clearDisplay();
    display.setTextSize(1);
    display.setTextColor(SSD1306_WHITE);
    display.setCursor(1, 1);
    display.println("Message:");
    display.setTextSize(2);
    display.print(msg);
    display.display();
    display.display();
    return RET_OK;
}