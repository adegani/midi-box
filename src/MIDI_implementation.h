// MIDI implementation

#define MIDI_SYSEX_ID 0x7D

#define MIDI_SYSEX_SHOW_MESSAGE             0x01

#define MIDI_SYSEX_PARAM_RS232_BAUD         0x02
#define MIDI_SYSEX_PARAM_RS232_POLARITY     0x03
#define MIDI_SYSEX_PARAM_EXT_POT_ENABLE     0x04
#define MIDI_SYSEX_PARAM_EXT_POT_CHANNEL    0x05
#define MIDI_SYSEX_PARAM_EXT_POT_BASE_CC    0x06
