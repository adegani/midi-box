#include "logger.hpp"

int32_t MIDIBOX_LOG_ERROR(const char* fmt, ...)
{
    if (MIDIBOX_LOG_LEVEL >= MIDIBOX_LOG_LEVEL_ERROR) {
        char buf[100];
        va_list vl;
        va_start(vl, fmt);
        vsnprintf( buf, sizeof( buf), fmt, vl);
        va_end( vl);

        Serial.print("ERROR: ");
        Serial.print(buf);
        Serial.print("\n");
    }
    return RET_OK;
}

int32_t MIDIBOX_LOG_INFO(const char* fmt, ...)
{
    if (MIDIBOX_LOG_LEVEL >= MIDIBOX_LOG_LEVEL_INFO) {
        char buf[100];
        va_list vl;
        va_start(vl, fmt);
        vsnprintf( buf, sizeof( buf), fmt, vl);
        va_end( vl);

        Serial.print("INFO: ");
        Serial.print(buf);
        Serial.print("\n");
    }
    return RET_OK;
}

int32_t MIDIBOX_LOG_DEBUG(const char* fmt, ...)
{
    if (MIDIBOX_LOG_LEVEL >= MIDIBOX_LOG_LEVEL_DEBUG) {
        char buf[100];
        va_list vl;
        va_start(vl, fmt);
        vsnprintf( buf, sizeof( buf), fmt, vl);
        va_end( vl);

        Serial.print("DEBUG: ");
        Serial.print(buf);
        Serial.print("\n");
    }
    return RET_OK;
}
