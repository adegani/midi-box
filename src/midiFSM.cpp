/* midiFSM.c
 *  MIDI Finite State Machine for MIDI message handle
 *  Author: Alessio Degani, 2017 <alessio.degani@gmail.com>
 */

#include "midiFSM.hpp"

static uint8_t sysexBuffer[MIDI_SYSEX_MAX_BYTES];
static uint16_t sysexByteCount = 0;
static bool runningSysex = false;

void midiFSM_reset(midiFSM_t *fsm)
{
    sysexByteCount = 0;
    runningSysex = false;
    fsm->state = RESET;
}

void midiFSM_init(midiFSM_t *fsm, void (*handleMessage)(midiFSM_event_t *e),
                    void (*handleSysex)(uint8_t *byte, uint16_t sysexByteCount, bool complete),
                    void (*handleRealTimeMessage)(uint8_t byte))
{
    fsm->state          = RESET;
    fsm->handleMessage  = handleMessage;
    fsm->handleSysex    = handleSysex;
    fsm->handleRealTimeMessage    = handleRealTimeMessage;
}

static void sysexAddBye(midiFSM_t *fsm, uint8_t byte)
{
    if ((sysexByteCount >= MIDI_SYSEX_MAX_BYTES-1) || (byte == MIDI_END_OF_SYSEX)) {
        sysexBuffer[sysexByteCount++] = byte;
        bool complete = (byte == MIDI_END_OF_SYSEX);
        if (fsm->handleSysex != NULL) {
            fsm->handleSysex(sysexBuffer, sysexByteCount, complete);
        }
        MIDIBOX_LOG_DEBUG("SYX END: %d", byte);
        sysexByteCount = 0;
    } else {
        MIDIBOX_LOG_DEBUG("SYX add: %d", byte);
        sysexBuffer[sysexByteCount++] = byte;
    }
}

static void dispatchMidiStatus(midiFSM_t *fsm, uint8_t byte)
{
    fsm->MIDIEvent.status = byte;
    uint8_t event = byte & 0xF0;
    switch (event) {
        // SYSTEM COMMON
        case MIDI_SYSTEM_COMMON:
            switch (byte) {
                // SYSEX
                case MIDI_SYSEX:
                    runningSysex = true;
                    sysexAddBye(fsm, byte);
                    fsm->state = WAIT_BYTE;
                    break;
                case MIDI_END_OF_SYSEX:
                    runningSysex = false;
                    sysexAddBye(fsm, byte);
                    break;
                // REAL TIME MSG (0 data bytes)
                case MIDI_TUNE_REQUEST:
                case MIDI_CLOCK:
                case MIDI_START:
                case MIDI_CONTINUE:
                case MIDI_STOP:
                case MIDI_ACTIVE_SENS:
                case MIDI_RESET_ALL:
                    if (fsm->handleRealTimeMessage != NULL) {
                        fsm->handleRealTimeMessage( byte );
                    }
                    break;
                // System with 1 data byte
                case MIDI_TIME_CODE:
                case MIDI_SONG_SELECT:
                    fsm->state = WAIT_DATA_BYTE;
                    fsm->MIDIEvent.type = byte;
                    break;
                // System with 2 data bytes
                case MIDI_SONG_POSITION:
                    fsm->state = WAIT_DATA_BYTE_1;
                    fsm->MIDIEvent.type = byte;
                    break;
            }
            break;
        // MIDI messages with 1 data byte
        case MIDI_PROGRAM_CHANGE:
        case MIDI_CHANNEL_PRESS:
            fsm->state = WAIT_DATA_BYTE;
            fsm->MIDIEvent.type = byte & 0xF0;
            fsm->MIDIEvent.channel = byte & 0x0F;
            break;
        // MIDI messages with 2 data bytes
        case MIDI_NOTE_OFF:
        case MIDI_NOTE_ON:
        case MIDI_POLY_KEY_PRESS:
        case MIDI_CONTROL_CHANGE:
        case MIDI_PITCH_BEND:
        default:
            fsm->state = WAIT_DATA_BYTE_1;
            fsm->MIDIEvent.type = byte & 0xF0;
            fsm->MIDIEvent.channel = byte & 0x0F;
    }
}

void dispatchMessage(midiFSM_t *fsm)
{
    switch ( fsm->MIDIEvent.status ){
        case MIDI_NOTE_OFF       :
        case MIDI_NOTE_ON        :
        case MIDI_POLY_KEY_PRESS :
        case MIDI_CONTROL_CHANGE :
        case MIDI_CHANNEL_PRESS  :
        case MIDI_PITCH_BEND     :
        case MIDI_PROGRAM_CHANGE :
            if (fsm->handleMessage != NULL) {
                fsm->handleMessage( &(fsm->MIDIEvent) );
            }
            break;
        case 0:
            break;
        default:
            if (fsm->handleMessage != NULL) {
                fsm->handleMessage( &(fsm->MIDIEvent) );
            }
            fsm->MIDIEvent.status = 0;
    }
}

void dispatchRealTimeMessage( midiFSM_t *fsm, uint8_t byte)
{
    fsm->handleRealTimeMessage(byte);
}

midiFSM_retVal midiFSM_handleByte(midiFSM_t *fsm, uint8_t byte)
{
    // MIDI FSM
    switch (fsm->state){
        case RESET:
        case WAIT_BYTE:
            if (byte & MIDI_STATUS) {
                dispatchMidiStatus(fsm, byte);
            } else if (runningSysex) {
                sysexAddBye(fsm, byte);
            }
            break;
        case WAIT_DATA_BYTE:
            if (byte & MIDI_STATUS) {
                dispatchMidiStatus(fsm, byte);
            } else {
                fsm->MIDIEvent.data1 = byte;
                fsm->MIDIEvent.data2 = 0;
                dispatchMessage(fsm);
            }
            break;
        case WAIT_DATA_BYTE_1:
            if (byte & MIDI_STATUS) {
                dispatchMidiStatus(fsm, byte);
            } else {
                fsm->MIDIEvent.data1 = byte;
                fsm->MIDIEvent.data2 = 0;
                fsm->state = WAIT_DATA_BYTE_2;
            }
            break;
        case WAIT_DATA_BYTE_2:
            if (byte & MIDI_STATUS) {
                dispatchMidiStatus(fsm, byte);
            } else {
                fsm->MIDIEvent.data2 = byte;
                fsm->state = WAIT_DATA_BYTE_1;
                dispatchMessage(fsm);
            }
            break;
        default:
            return FSM_RET_ERROR;
    }
    return FSM_RET_OK;
}
