#include "paramRegistry.hpp"

static bool paramsDirty = false;
uint8_t paramValues[PARAM_REG_NUMPARAM];

void writeEEPROM()
{
    if (paramsDirty)
    {
        uint8_t paramValue;
        uint8_t checksum = 0;
        for (uint32_t i = 0; i < PARAM_REG_NUMPARAM; i++)
        {
            paramValue = EEPROM.read(i+1);
            checksum += paramValues[i];
            if (paramValue != paramValues[i])
            {
                EEPROM.write(i+1, paramValues[i]);
            }
        }
        EEPROM.write(0, checksum);
        paramsDirty = false;
    }
}

int32_t paramReg_getParamValue(uint32_t paramId)
{
    return paramValues[paramId];
}

int32_t paramReg_setParamValue(uint32_t paramId, uint32_t paramValue)
{
    paramValues[paramId] = paramValue;
    paramsDirty = true;
    return 0;
}

int32_t paramReg_formatEEPROM()
{
    EEPROM.write(PARAM_MODE_IDX+1, PARAM_MODE_DEFAULT);
    EEPROM.write(PARAM_MONITOR_IDX+1, PARAM_MONITOR_DEFAULT);
    EEPROM.write(PARAM_METRONOME_IDX+1, PARAM_METRONOME_DEFAULT);
    EEPROM.write(PARAM_MESSAGE_IDX+1, PARAM_MESSAGE_DEFAULT);
    EEPROM.write(PARAM_BAUD_IDX+1, PARAM_BAUD_DEFAULT);
    EEPROM.write(PARAM_POLARITY_IDX+1, PARAM_POLARITY_DEFAULT);
    EEPROM.write(PARAM_POT_EN_IDX+1, PARAM_POT_EN_DEFAULT);
    EEPROM.write(PARAM_POT_CH_IDX+1, PARAM_POT_CH_DEFAULT);
    EEPROM.write(PARAM_POT_BASE_IDX+1, PARAM_POT_BASE_DEFAULT);
    
    uint8_t checksum = 0;
    for (uint32_t i = 0; i < PARAM_REG_NUMPARAM; i++)
    {
        paramValues[i] = EEPROM.read(i+1);
        checksum += paramValues[i];
    }
    EEPROM.write(0, checksum);
    return 0;
}

int32_t paramReg_init()
{
    int32_t retVal = 0;
    uint8_t checksum = 0;
    for (uint32_t i = 0; i < PARAM_REG_NUMPARAM; i++)
    {
        paramValues[i] = EEPROM.read(i+1);
        checksum += paramValues[i];
    }
    uint8_t eepromChecksum = EEPROM.read(0);
    
    if (checksum != eepromChecksum)
    {
        retVal = -1;
    }
    return retVal;
}
