#ifndef MIDIBOX_LOGGER_H
#define MIDIBOX_LOGGER_H

#include <Arduino.h>
#include <stdarg.h>
#include <stdio.h>
#include "config.h"

#define MIDIBOX_LOG_LEVEL_NONE  0
#define MIDIBOX_LOG_LEVEL_ERROR 1
#define MIDIBOX_LOG_LEVEL_INFO  2
#define MIDIBOX_LOG_LEVEL_DEBUG 3

#ifndef MIDIBOX_LOG_LEVEL
#define MIDIBOX_LOG_LEVEL MIDIBOX_LOG_LEVEL_ERROR
#endif

#ifdef __cplusplus
extern "C" {
#endif
    int32_t MIDIBOX_LOG_ERROR(const char* fmt, ...);
    int32_t MIDIBOX_LOG_INFO(const char* fmt, ...);
    int32_t MIDIBOX_LOG_DEBUG(const char* fmt, ...);
#ifdef __cplusplus
}
#endif

#endif // MIDIBOX_LOGGER_H
